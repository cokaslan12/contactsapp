//
//  ContactsAppApp.swift
//  ContactsApp
//
//  Created by Muzaffer Çokaslan on 19.11.2023.
//

import SwiftUI

@main
struct ContactsAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

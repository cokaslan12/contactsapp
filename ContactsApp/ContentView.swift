//
//  ContentView.swift
//  ContactsApp
//
//  Created by Muzaffer Çokaslan on 19.11.2023.
//

import SwiftUI

enum SectionType:String{
    case ceo, employee, peasants
}

class Contact: NSObject{
    let name:String
    var isFavorite:Bool = false
    
    init(name: String) {
        self.name = name
    }
}


class ContactViewModel: ObservableObject{
    @Published var name = ""
    @Published var isFavorite:Bool = false
}

struct ContactRowView:  View {
    @ObservedObject var vm:ContactViewModel
    var body: some View {
        HStack(spacing:16,  content: {
            Image(systemName: "person.fill")
            Text(vm.name)
            Spacer()
            Image(systemName: vm.isFavorite ? "star.fill" :"star" )
        })
        .padding()
    }
}


class ContactCell:UITableViewCell{
    let viewModel = ContactViewModel()
    lazy var row = ContactRowView(vm: viewModel)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
       //setup my ui somehow...
        let hostingController = UIHostingController(rootView: row)
        self.addSubview(hostingController.view)
               hostingController.view.translatesAutoresizingMaskIntoConstraints = false
        hostingController.view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        hostingController.view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        hostingController.view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        hostingController.view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class ContactsSource: UITableViewDiffableDataSource<SectionType, Contact>{
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

class DiffableTableViewController: UITableViewController{
    
    //UITableViewDiffableDataSource
    lazy var source: ContactsSource = .init(tableView: self.tableView) { tableView, indexPath, contact in
        
        let cell = ContactCell(style: .default, reuseIdentifier: nil)
        cell.viewModel.name = contact.name
        cell.viewModel.isFavorite = contact.isFavorite
        return cell
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { action, view, completion in
            
            completion(true)
            
            var snapshot = self.source.snapshot()
            
            //figure out the contact we need to delete
            guard let contact = self.source.itemIdentifier(for: indexPath) else {return}
            
            snapshot.deleteItems([contact])
            
            self.source.apply(snapshot)
            
        }
        
        let favoriteAction = UIContextualAction(style: .normal, title: "Favorite") { _, _, completion in
            completion(true)
            //tricky tricky tricky
            
            var snapshot = self.source.snapshot()
            
            //figure out the contact we need to delete
            guard var contact = self.source.itemIdentifier(for: indexPath) else {return}
            
            contact.isFavorite.toggle()
            
            snapshot.reloadItems([contact])
            
            self.source.apply(snapshot)
        }
        
        return .init(actions: [deleteAction, favoriteAction])
    }
    
    
    private func setupSource(){
        var snapshot = source.snapshot()
        snapshot.appendSections([.ceo, .peasants, .employee])
        
        snapshot.appendItems(
            [
              .init(name: "Tim Cook"),
              .init(name: "Elon Musk"),
              .init(name: "Muzaffer Çokaslan")
        ], toSection: .ceo)
        
        snapshot.appendItems([
            .init(name: "Brian Voong")
        ], toSection: .peasants)
        
        source.apply(snapshot)
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text =  section == 0 ? "CEO" : "Peasant"
        
        return label
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Contacts"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = .init(title: "Add", style: .plain, target: self, action: #selector(handleAdd))
        setupSource()
    }
    
    @objc private func handleAdd(){
        let formView = ContactFormView { name, type in
            self.dismiss(animated: true)
        
            var snapshot = self.source.snapshot()
            snapshot.appendItems([.init(name: name)], toSection: type)
            self.source.apply(snapshot)
            
        }
        let hostingController = UIHostingController(rootView: formView)
        
        present(hostingController, animated: true)
        //add a sample contact
        
    }
}

struct ContactFormView: View {
    var didAddContact:(String, SectionType) -> () = { _, _ in}
    @State var name:String = ""
    @State var selectedItem:SectionType = SectionType.ceo
    
    var body: some View {
        VStack(spacing:20, content: {
            VStack(alignment: .center, spacing: 10, content: {
                Circle()
                    .fill(.white)
                    .overlay(alignment: .center) {
                        Image(systemName: "person.fill")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30, height: 30)
                    }
                    .frame(width: 100, height: 100)
                    .background(
                        Circle()
                            .stroke(.black, lineWidth: 4)
                    )
                Button("Select Image") {
                    
                }
            })
            .frame(maxWidth: .infinity)
            TextField("Name", text: $name)
            Picker("Select", selection: $selectedItem) {
                ForEach([SectionType.ceo, SectionType.employee, SectionType.peasants], id: \.self) { element in
                    Text(element.rawValue.capitalized)
                        .tag(element)
                }
            }
            .pickerStyle(.segmented)
            
            Button(action: {
                //run a function/closure somehow
                self.didAddContact(self.name, self.selectedItem)
            }, label: {
                Text("Add")
                    .foregroundStyle(.white)
                    .fontWeight(.bold)
                    .font(.system(size: 20))
                    .frame(height: 45)
                    .frame(maxWidth: .infinity)
                    .background(.blue)
                    .clipShape(RoundedRectangle(cornerRadius: 5))
            })
            Spacer()
        })
        .padding()
    }
}


struct DiffableContainer: UIViewControllerRepresentable{
    func makeUIViewController(context: Context) -> UIViewController {
        return UINavigationController(rootViewController: DiffableTableViewController(style: .insetGrouped))
    }
    
    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        
    }
    
    typealias UIViewControllerType = UIViewController
    
    
}


struct ContentView: View {
    var body: some View {
       Text("Hello world")
    }
}

#Preview {
    DiffableContainer()
}
